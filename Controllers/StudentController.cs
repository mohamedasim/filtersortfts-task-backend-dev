using System;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using SortFilterWithSql.Models;
namespace SortFilterWithSql.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        IConfiguration Configure; 

         public StudentController (IConfiguration configuration){
             Configure=configuration;
         }

        [HttpGet("list")]
        public ActionResult Get(){
            string query= @"select * from Students";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table) ;
        }
        [HttpGet("single/{id}")]
        public ActionResult GetSingleById(int id){
             string query= @"select * from Students where Id ='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult(table) ;
        }

        [HttpPost("create")]
        public ActionResult Post(StudentModel student){
             string query= @"insert into  Students values('"+student.Name+ @"','"+student.Department+ @"','"+student.Gender+ @"','"+student.DateOfBirth+ @"','"+student.MarksPercentage+ @"')";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Created Successfully!") ;
        }

        [HttpPut("update/{id}")]
        public ActionResult Put(StudentModel student,int id){
             string query= @"update Students set Name ='"+student.Name+ @"',Department='"+student.Department+ @"',Gender='"+student.Gender+ @"',DateOfBirth='"+student.DateOfBirth+ @"',MarksPercentage='"+student.MarksPercentage+ @"'where Id='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Updated Successfully!") ;
        }

        [HttpDelete("delete/{id}")]
        public ActionResult Delete(int id){
             string query= @"delete from Students where Id='"+id+@"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Requested Data has been deleted Successfully!") ;
        }
        [HttpGet("list/sorting")]
        public ActionResult GetSort(string filterByName,string orderType){
            string query= @"select  * from Students order by  "+filterByName+" "+orderType+"";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table) ;
        }
        [HttpGet("list/filter")]
        public ActionResult GetFilter(string filterByName,string ordeByValue){
            string query= @"select  * from Students  where  "+filterByName+" like '"+ordeByValue+"%'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table) ;
        }
    }
}